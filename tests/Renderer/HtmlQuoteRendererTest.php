<?php

declare(strict_types=1);

namespace Test\Quote\Renderer;

use PHPUnit\Framework\TestCase;
use Quote\Model\AttributableQuote;
use Quote\Model\AuthorName;
use Quote\Model\Message;
use Quote\Renderer\HtmlQuoteRenderer;

/**
 * @coversDefaultClass \Quote\Renderer\HtmlQuoteRenderer
 */
class HtmlQuoteRendererTest extends TestCase
{
    private $quote;

    public function setUp() : void
    {
        $msg = new Message('this is my message');
        $author = new AuthorName('Tom');

        $this->quote = new AttributableQuote($msg, $author);
    }

    /**
     * @covers ::__toString
     */
    public function testToStringReturnsString() : void
    {
        $renderer = new HtmlQuoteRenderer($this->quote);

        $this->assertInternalType(
            'string',
            (string) $renderer
        );
    }

    /**
     * @covers ::__toString
     */
    public function testToStringReturnsHtml() : void
    {
        $renderer = new HtmlQuoteRenderer($this->quote);

        $this->assertStringStartsWith(
            '<blockquote><p>',
            (string) $renderer
        );

        $this->assertStringEndsWith(
            '</p></blockquote>',
            (string) $renderer
        );
    }
}
