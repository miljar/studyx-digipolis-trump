<?php

declare(strict_types=1);

namespace Quote;

use Quote\QuoteGenerator\HtmlQuoteGenerator;
use Quote\QuoteGenerator\PlainTextQuoteGenerator;
use Quote\QuoteSelector\Last;
use Quote\QuoteSelector\Random;
use Quote\QuoteSource\Trump;

/**
 * Class: TrumpFactory
 *
 * @see QuoteGeneratorFactory
 * @final
 */
final class TrumpFactory extends QuoteGeneratorFactory
{
    public const LAST = 'last';
    public const RANDOM = 'random';

    private const HTML = 'html';
    private const PLAIN = 'plain';

    /**
     * @param string $type
     * @param string $renderer
     *
     * @return QuoteGenerator
     */
    private function createGenerator(string $type, string $renderer) : QuoteGenerator
    {
        $selector = null;

        switch ($type) {
            case self::LAST:
                $selector = new Last();
                break;
            default:
                $selector = new Random();
                break;
        }

        switch ($renderer) {
            case self::HTML;
                return new HtmlQuoteGenerator(
                    new Trump($selector)
                );
            case self::PLAIN;
            default:
                return new PlainTextQuoteGenerator(
                    new Trump($selector)
                );
        }

    }

    /**
     * {@inheritdoc}
     */
    public function create(string $type) : QuoteGenerator
    {
        return self::createGenerator(
            $type,
            self::PLAIN
        );
    }

    /**
     * {@inheritdoc}
     */
    public function createHtml(string $type) : QuoteGenerator
    {
        return self::createGenerator(
            $type,
            self::HTML
        );
    }
}
