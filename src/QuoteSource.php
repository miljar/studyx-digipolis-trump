<?php

declare(strict_types=1);

namespace Quote;

use Quote\Model\Quote;

/**
 * Interface: QuoteSource
 */
interface QuoteSource
{
    /**
     * Retrieves a quote
     *
     * @return Quote
     */
    public function get() : Quote;
}
