<?php

declare(strict_types=1);

namespace Quote;

use Quote\Model\Quote;

/**
 * Interface: QuoteGenerator
 */
interface QuoteGenerator
{
    /**
     * @return Quote
     */
    public function retrieve() : Quote;

    /**
     * @return QuoteSource
     */
    public function source() : QuoteSource;

    /**
     * Change the current source of quotes
     *
     * @param QuoteSource $source
     *
     * @return QuoteGenerator
     */
    public function changeSource(QuoteSource $source) : QuoteGenerator;
}
