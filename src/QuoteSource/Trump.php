<?php

declare(strict_types=1);

namespace Quote\QuoteSource;

use Quote\Model\AttributableQuote;
use Quote\Model\AuthorName;
use Quote\Model\Message;
use Quote\Model\Quote;
use Quote\QuoteSelector;
use Quote\QuoteSource;

/**
 * Class: Trump
 *
 * @see QuoteSource
 * @final
 */
final class Trump implements QuoteSource
{
    /**
     * @var QuoteSelector
     */
    private $quoteSelector;

    /**
     * @param QuoteSelector $selector
     */
    public function __construct(
        QuoteSelector $selector
    ) {
        $this->quoteSelector = $selector;
    }

    /**
     * @param QuoteSelector $selector
     *
     * @return Trump
     */
    public function changeSelector(QuoteSelector $selector) : self
    {
        $this->quoteSelector = $selector;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function get() : Quote
    {
        $url = $this->getUrl();
        $messages = json_decode(file_get_contents($url))->messages->non_personalized;

        $message = $this->quoteSelector->select($messages);

        $quote = new AttributableQuote(
            new Message($message),
            new AuthorName('Donald Trump')
        );

        return $quote;
    }

    /**
     * @return string
     */
    private function getUrl() : string
    {
        $baseUrl = 'https://api.whatdoestrumpthink.com/api/';
        $path    = 'v1/quotes';

        return sprintf('%1$s%2$s', $baseUrl, $path);
    }
}
