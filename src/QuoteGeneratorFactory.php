<?php

declare(strict_types=1);

namespace Quote;

/**
 * Class: QuoteGeneratorFactory
 *
 * @abstract
 */
abstract class QuoteGeneratorFactory
{
    /**
     * Creates a new QuoteGenerator instance
     *
     * @param string $type
     *
     * @return QuoteGenerator
     */
    abstract public function create(string $type) : QuoteGenerator;

    /**
     * Creates a new QuoteGenerator instance
     *
     * @param string $type
     *
     * @return QuoteGenerator
     */
    abstract public function createHtml(string $type) : QuoteGenerator;
}
