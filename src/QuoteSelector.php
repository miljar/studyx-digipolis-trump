<?php

declare(strict_types=1);

namespace Quote;

/**
 * Interface: QuoteSelector
 */
interface QuoteSelector
{
    /**
     * Selects a quote
     *
     * @param array $list
     *
     * @return string
     */
    public function select(array $list) : string;
}
