<?php

declare(strict_types=1);

namespace Quote\QuoteSelector;

use Quote\QuoteSelector;

/**
 * Class: Random
 *
 * @see QuoteSelector
 * @final
 */
final class Random implements QuoteSelector
{
    /**
     * {@inheritdoc}
     */
    public function select(array $list) : string
    {
        return $list[mt_rand(0, count($list) - 1)];
    }
}
