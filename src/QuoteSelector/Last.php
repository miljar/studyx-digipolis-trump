<?php

declare(strict_types=1);

namespace Quote\QuoteSelector;

use Quote\QuoteSelector;

/**
 * Class: Last
 *
 * @see QuoteSelector
 * @final
 */
final class Last implements QuoteSelector
{
    /**
     * {@inheritdoc}
     */
    public function select(array $list) : string
    {
        return end($list);
    }
}
