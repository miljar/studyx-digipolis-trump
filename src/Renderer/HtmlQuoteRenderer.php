<?php

declare(strict_types=1);

namespace Quote\Renderer;

/**
 * Class: HtmlQuoteRenderer
 *
 * @see BaseQuoteRenderer
 * @final
 */
final class HtmlQuoteRenderer extends BaseQuoteRenderer
{
    /**
     * {@inheritdoc}
     */
    public function __toString() : string
    {
        return sprintf(
            '<blockquote><p>%1$s</p></blockquote>',
            (string) $this->wrapped
        );
    }
}
