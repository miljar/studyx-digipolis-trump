<?php

declare(strict_types=1);

namespace Quote\Renderer;

use Quote\Model\Quote;

/**
 * Class: BaseQuoteRenderer
 *
 * @see Quote
 * @abstract
 */
abstract class BaseQuoteRenderer implements Quote
{
    /**
     * @var Quote
     */
    protected $wrapped;

    /**
     * @param Quote $wrapped
     */
    public function __construct(Quote $wrapped)
    {
        $this->wrapped = $wrapped;
    }
}
