<?php

declare(strict_types=1);

namespace Quote\Renderer;

/**
 * Interface: Renderable
 */
interface Renderable
{
    /**
     * Returns a string representation
     *
     * @return string
     */
    public function __toString() : string;
}
