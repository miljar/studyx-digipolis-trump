<?php

declare(strict_types=1);

namespace Quote\Model;

use Quote\Renderer\Renderable;

/**
 * Interface: Quote
 *
 * @see Renderable
 */
interface Quote extends Renderable/*, HasMessage*/
{
}
