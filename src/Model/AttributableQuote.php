<?php

declare(strict_types=1);

namespace Quote\Model;

use InvalidArgumentException;
use Quote\Renderer\Renderable;

/**
 * Class: AttributableQuote
 *
 * @see Quote
 * @final
 */
final class AttributableQuote implements Quote
{
    /**
     * @var Message
     */
    private $message;

    /**
     * @var AuthorName
     */
    private $author;

    /**
     * @param Message $message
     * @param AuthorName $author
     */
    public function __construct(Message $message, AuthorName $author)
    {
        $this->message = $message;
        $this->author = $author;
    }

    /**
     * {@inheritDoc}
     */
    public function __toString() : string
    {
        return (string) $this->message;
    }
}
