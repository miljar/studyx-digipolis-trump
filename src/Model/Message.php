<?php

declare(strict_types=1);

namespace Quote\Model;

use InvalidArgumentException;
use Quote\Renderer\Renderable;

/**
 * Class: Message
 *
 * @see Renderable
 * @final
 */
final class Message implements Renderable
{
    /**
     * @var string
     */
    private $message;

    /**
     * @param string $message
     */
    public function __construct(string $message)
    {
        if (strlen($message) === 0) {
            throw new InvalidArgumentException('Message provided was too short');
        }

        $this->message = $message;
    }

    /**
     * Determines if given other instance has same value
     * as current instance
     *
     * @param Message $other
     *
     * @return bool
     */
    public function equals(Message $other) : bool
    {
        return ($other->message === $this->message);
    }

    /**
     * {@inheritDoc}
     */
    public function __toString() : string
    {
        return $this->message;
    }
}
