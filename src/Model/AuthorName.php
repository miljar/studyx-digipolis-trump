<?php

declare(strict_types=1);

namespace Quote\Model;

use InvalidArgumentException;

/**
 * Class: AuthorName
 *
 * @final
 */
final class AuthorName
{
    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        if (strlen($name) === 0) {
            throw new InvalidArgumentException('Name provided was too short');
        }

        if (strlen($name) > 100) {
            throw new InvalidArgumentException('Name provided was too long');
        } 

        $this->name = $name;
    }

    /**
     * Verifies if given other instance equals current instance
     *
     * @param AuthorName $other
     *
     * @return bool
     */
    public function equals(AuthorName $other) : bool
    {
        return ($other->name === $this->name);
    }
}
