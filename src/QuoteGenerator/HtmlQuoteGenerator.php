<?php

declare(strict_types=1);

namespace Quote\QuoteGenerator;

use Quote\Model\Quote;
use Quote\Renderer\HtmlQuoteRenderer;

/**
 * Class: HtmlQuoteGenerator
 *
 * @see BaseQuoteGenerator
 * @final
 */
final class HtmlQuoteGenerator extends BaseQuoteGenerator
{
    /**
     * {@inheritdoc}
     */
    public function retrieve() : Quote
    {
        $decorator = new HtmlQuoteRenderer(
            parent::retrieve()
        );

        return $decorator;
    }
}
