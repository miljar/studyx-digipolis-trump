<?php

declare(strict_types=1);

namespace Quote\QuoteGenerator;

use Quote\Model\Quote;
use Quote\QuoteGenerator;
use Quote\QuoteSource;

/**
 * Class: BaseQuoteGenerator
 *
 * @see QuoteGenerator
 * @abstract
 */
abstract class BaseQuoteGenerator implements QuoteGenerator
{
    /**
     * @var QuoteSource
     */
    protected $source;

    /**
     * @param QuoteSource $source
     */
    public function __construct(QuoteSource $source)
    {
        $this->source = $source;
    }

    /**
     * @return QuoteSource
     */
    public function source() : QuoteSource
    {
        return $this->source;
    }

    /**
     * {@inheritdoc}
     */
    public function changeSource(QuoteSource $source) : QuoteGenerator
    {
        $this->source = $source;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function retrieve() : Quote
    {
        return $this->source->get();
    }
}
