<?php

declare(strict_types=1);

namespace Quote\QuoteGenerator;

use Quote\Quote;

/**
 * Class: PlainTextQuoteGenerator
 *
 * @see BaseQuoteGenerator
 * @final
 */
final class PlainTextQuoteGenerator extends BaseQuoteGenerator
{
}
