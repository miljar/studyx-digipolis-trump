<?php

require __DIR__ . '/../vendor/autoload.php';

use Quote\TrumpFactory;

$plainTextGenerator = (new TrumpFactory())->create(TrumpFactory::RANDOM);
$htmlGenerator = (new TrumpFactory())->createHtml(TrumpFactory::RANDOM);

var_dump((string) $plainTextGenerator->retrieve());
var_dump((string) $htmlGenerator->retrieve());




/*
use Quote\QuoteGenerator\HtmlQuoteGenerator;
use Quote\QuoteGenerator\PlainTextQuoteGenerator;
use Quote\QuoteSelector\Last;
use Quote\QuoteSelector\Random;
use Quote\QuoteSource\Trump;

$random        = new Random();
$last          = new Last();
$source        = new Trump($last);
$generator     = new PlainTextQuoteGenerator($source);

var_dump((string) $generator->retrieve());
var_dump((string) $generator->retrieve());

$generator->source()->changeSelector($random);

var_dump('* * * * * * *');

var_dump((string) $generator->retrieve());
var_dump((string) $generator->retrieve());
 */
